﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class ARTapToPlaceObject : MonoBehaviour
{
    public GameObject placementIndicator;
    public GameObject targetModel;
    public Text m_Logs;
    private ARRaycastManager m_RaycastManager;
    private bool placementPoseIsValid = false;
    private Pose placementPose;
    // Start is called before the first frame update
    void Start()
    {
        m_RaycastManager = GetComponent<ARRaycastManager>();
        

    }

    // Update is called once per frame
    void Update()
    {
        UpdatePlacementPose();
        UpdatePlacementIndicator();
        HandleTaps();

        
        
        
    }

    private void HandleTaps()
    {
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            AddLog("Touch");
        }
        if (placementPoseIsValid && Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
        {

            Collider[] hitColliders = Physics.OverlapSphere(placementPose.position, 0.05f);
            AddLog("hits " + hitColliders.Length);
            if (hitColliders.Length == 0) 
                Instantiate(targetModel, placementPose.position, placementPose.rotation);
        }
    }

    private void UpdatePlacementIndicator()
    {
        if (placementPoseIsValid)
        {
            placementIndicator.SetActive(true);
            placementIndicator.transform.SetPositionAndRotation(placementPose.position, placementPose.rotation);
        } else
        {
            placementIndicator.SetActive(false);
        }
        
    }

    private void UpdatePlacementPose()
    {
        var hits = new List<ARRaycastHit>();
        if (Camera.current == null)
            return;
        m_RaycastManager.Raycast(Camera.current.ViewportToScreenPoint(new Vector3(0.5f, 0.5f)), hits);
        placementPoseIsValid = hits.Count > 0;
        if (placementPoseIsValid)
        {
            placementPose = hits[0].pose;

        } 
    }
    private void AddLog(string msg)
    {
        m_Logs.text = m_Logs.text + msg+ "\n";
    }


}
