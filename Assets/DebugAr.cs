﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugAr : MonoBehaviour
{
    public GameObject mEarth;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        var cam = GetComponent<Camera>();
        var pos = cam.ViewportToScreenPoint(new Vector3(0.5f, 0.5f));
        RaycastHit hits;
        
        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("touch");
            var ray = cam.ScreenPointToRay(Input.mousePosition);
            if (!Physics.Raycast(ray, out hits)) {
                Instantiate(mEarth, new Vector3(0, 0, 2), new Quaternion());
            }
        }
        
    }
}
